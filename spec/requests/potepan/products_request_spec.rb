require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  describe "get #show" do
    let(:taxon_1) { create(:taxon) }
    let(:taxon_2) { create(:taxon) }
    let(:product_1) { create(:product, name: 'test_product_1', taxons: [taxon_1]) }
    let!(:product_2) { create(:product, name: 'test_product_2', taxons: [taxon_1]) }
    let(:product_3) { create(:product, name: 'test_product_3', taxons: [taxon_2]) }

    before do
      get potepan_product_url product_1.id
    end

    it "return response successful" do
      expect(response.status).to eq 200
    end

    it "display product's name" do
      expect(response.body).to include 'test_product_1'
    end

    it "display related product's name" do
        expect(response.body).to include 'test_product_2'
        expect(response.body).to_not include 'test_product_3'
    end
  end
end
