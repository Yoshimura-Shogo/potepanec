require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  let!(:taxonomy) { FactoryBot.create(:taxonomy, name: "Category") }
  let!(:taxon) { FactoryBot.create(:taxon, taxonomy: taxonomy, name: "Bags") }
  let!(:product) { FactoryBot.create(:product, taxons: [taxon], name: "Test Bag") }

  before do
    get potepan_category_path(taxon_id: taxon.id)
  end

  it "responds successfully" do
    expect(response).to have_http_status "200"
  end

  it "display the taxonomy, taxon, and product" do
    expect(response.body).to include "Category"
    expect(response.body).to include "Bags"
    expect(response.body).to include "Test Bag"
  end
end
