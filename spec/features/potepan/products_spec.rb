require 'rails_helper'

RSpec.feature 'product page', type: :feature do
  let(:taxon_1) { create(:taxon) }
  let(:taxon_2) { create(:taxon) }
  let(:product_1) { create(:product, taxons: [taxon_1]) }
  let!(:product_2) { create(:product, taxons: [taxon_1]) }

  background do
    visit potepan_product_path product_1.id
  end

  scenario "product page has correct texts" do
    expect(page).to have_title "#{product_1.name} | BIG BAG"
    within(".breadcrumb") do
      expect(page).to have_link "HOME"
      expect(page).to have_content product_1.name
    end
    within(".navbar-right") do
      expect(page).to have_link "Home"
    end
    within(".media-body") do
      expect(page).to have_content product_1.name
      expect(page).to have_content product_1.description
      expect(page).to have_content product_1.display_price
    end
  end

  scenario "logo image has link to root" do
    click_on 'logo'
    expect(current_path).to eq potepan_root_path
  end

  scenario "product page has related products" do
    within(".productBox") do
      expect(page).to have_content product_2.name
    end
  end

  scenario "related product has link to its show page" do
    click_on product_2.name
    expect(current_path).to eq potepan_product_path(product_2.id)
  end

  scenario "「一覧ページへ戻る」has link to category page" do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product_1.taxons.first.id)
  end
end
