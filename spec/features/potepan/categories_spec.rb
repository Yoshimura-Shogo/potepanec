require 'rails_helper'

RSpec.feature 'category page', type: :feature do
  let!(:taxonomy) { FactoryBot.create(:taxonomy) }
  let!(:taxon) { FactoryBot.create(:taxon, taxonomy: taxonomy) }
  let!(:product_1) { FactoryBot.create(:product, taxons: [taxon]) }
  let!(:product_2) { FactoryBot.create(:product) }

  before do
    visit potepan_category_path(taxon_id: taxon.id)
  end

  scenario "visit category page" do
    expect(page).to have_title "#{taxon.name} | BIG BAG"
  end

  scenario "category contains products" do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content taxon.products.size
    expect(page).to have_content product_1.name
    expect(page).to have_content product_1.display_price
    expect(page).to_not have_content product_2.name
  end

  scenario "link to product page" do
    click_link product_1.name
    expect(page).to have_current_path(potepan_product_path(product_1.id))
  end
end
