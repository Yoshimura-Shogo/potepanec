require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:related_product) { create(:product, taxons: [taxon]) }

  describe "related products" do
    it "matches related products" do
      expect(product.related_products).to contain_exactly(related_product)
      expect(product.related_products).not_to contain_exactly(product)
    end
  end
end
