class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_SIZE = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.limit(RELATED_PRODUCTS_SIZE)
  end
end
